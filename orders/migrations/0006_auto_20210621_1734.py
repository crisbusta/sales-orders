# Generated by Django 3.1.7 on 2021-06-21 21:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0005_auto_20210615_1853'),
    ]

    operations = [
        migrations.AlterField(
            model_name='detail',
            name='product',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='product', to='orders.product'),
        ),
    ]
