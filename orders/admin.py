from django.contrib import admin
from orders.models import Sale, Product, Detail, UserProfile, Sucursal, Client
from .forms import *
#admin.site.site_header= 'Admin XD'

# Register your models here.

@admin.register(Sale)
class SaleAdmin(admin.ModelAdmin):
    list_display = ['id', 'date', 'client', 'total']

@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    search_fields = ['name']
    form = DetailForm

@admin.register(Detail)
class DetailAdmin(admin.ModelAdmin):
    list_display = ['Numero_boleta', 'quantity']
    autocomplete_fields = ['product']

@admin.register(UserProfile)
class UserProfileAdmin(admin.ModelAdmin):
    list_display = ['user']

@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ['codigo','name']

@admin.register(Sucursal)
class SucursalAdmin(admin.ModelAdmin):
    list_display = ['codigo_sucursal', 'name', 'direccion', 'client']