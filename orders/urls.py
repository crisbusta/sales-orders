from django.urls import path, include
from orders import views
from orders.models import Product, Detail
from django.conf.urls import url
from .views import *
app_name = 'orders'

urlpatterns = [
    path('', views.home, name='home'),
    path('crear/', views.createOrder, name='create'),
    path('ordenes/', views.listOrder, name='list-orders'),
    path('editar-orden/<int:id>', views.updateOrder, name='update-order'),
    path('crear-detalle/<int:id>', views.createDetail, name='create-detail'),
    path('editar-detalle/<int:id>', views.updateDetail, name='update-detail'),
    path('detalles/<int:id>', views.listDetail, name='list-detail'),
    path('eliminar-detalle/<int:id>', views.deleteDetail, name='delete-detail'),
    path('export_csv/', views.export_csv, name='export-csv'),
    path('export_pdf/', views.render_to_pdf, name="export-pdf" ),
    path('checkout/<int:id>/', views.sendMail, name='send-email'),
    path('client-json/', get_json_client_data, name='client-json'),
    path('ajax/load-cities/', views.load_cities, name='ajax_load_cities'), # AJAX
]