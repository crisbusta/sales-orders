# Generated by Django 3.1.7 on 2021-06-13 22:23

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0003_auto_20210612_1804'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='client',
            name='sucursales',
        ),
        migrations.AddField(
            model_name='sucursal',
            name='client',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='orders.client'),
        ),
    ]
