from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save

GENDERS = (
    ('H', 'Hombre'),
    ('M', 'Mujer'),
    ('O', 'Otro'),
)

class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    address = models.CharField(max_length=100, default='No indica')
    gender = models.CharField(max_length=1, choices=GENDERS, default='N')
    city = models.CharField(max_length=50, default='No indica')
    phone = models.IntegerField(default=0)
    
    def __str__(self):
        return self.user.first_name

# Función que se ejecuta cuando se crea un usuario nuevo, realiza un trigger que asocia ese usuario nuevo a un perfil UserProfile
def create_profile(sender, **kwargs):
    if kwargs['created']:
        user_profile = UserProfile.objects.create(user=kwargs['instance'])
post_save.connect(create_profile, sender=User)

class Client(models.Model):
    name = models.CharField(max_length=50)
    codigo = models.IntegerField()
    def __str__(self):
        return self.name

class Sucursal(models.Model):
    name = models.CharField(max_length=50)
    direccion = models.CharField(max_length=100)
    codigo_sucursal = models.IntegerField()
    client = models.ForeignKey(Client, on_delete=models.CASCADE, default=1)
    def __str__(self):
        return self.name

class Product(models.Model):
    name = models.CharField(max_length=50)
    codigo = models.IntegerField()
    def __str__(self):
        return self.name
        
class Sale(models.Model):
    date = models.DateField()
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    seller = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
    sucursal = models.ForeignKey(Sucursal, on_delete=models.CASCADE)
    total = models.PositiveIntegerField()

    def __str__(self):
        return str(self.id)

class Detail(models.Model):
    Numero_boleta = models.ForeignKey(Sale, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='product')
    quantity = models.PositiveIntegerField()
    
    def __str__(self):
        return str(self.Numero_boleta)
        