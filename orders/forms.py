from django import forms
from orders.models import Sale, Detail, Product, Client


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = '__all__'


class DetailForm(forms.ModelForm):
    class Meta:
        model = Detail
        fields = ('__all__')
        exclude = ('Numero_boleta',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if 'product' in self.data:
            self.fields['product'].queryset = Product.objects.all()
        elif self.instance.pk:
            self.fields['product'].queryset = Product.objects.all().filter(id=self.instance.product.pk)


class SaleForm(forms.ModelForm):
    class Meta:
        model = Sale
        fields = '__all__'
        exclude = ('date', 'total', 'seller')
