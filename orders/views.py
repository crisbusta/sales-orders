import json
import csv
import datetime
import io as BytesIO
from django.core.exceptions import SuspiciousFileOperation
from django.core.files.base import ContentFile
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from orders.forms import ProductForm, SaleForm, DetailForm
from orders.models import Sale, UserProfile, Detail, Client, Sucursal, Product
from datetime import date
from django.template.loader import render_to_string, get_template
from django.db.models import Q
from django.core.mail import send_mail, EmailMultiAlternatives
from orders.utils import render_to_pdf


# Create your views here.


def home(request):
    template_name = 'home.html'
    data = {}
    return render(request, template_name, data)


def createOrder(request):
    data = {}
    template_name = 'create-orden.html'
    #Obtengo al usuario
    user = UserProfile.objects.get(user=request.user.id)
    form = SaleForm(request.POST or None, request.FILES or None)
    if request.is_ajax(): #Si la request viene de ajax
        term = request.GET.get('term') #Obtengo el termino, que es un input del usuario
        clients = Client.objects.all().filter(name__icontains=term) #Realizo una consulta en los productos buscando el input del usuario
        return JsonResponse(list(clients.values()), safe=False)

    if request.method == 'POST': #Si el metodo es POST
        #Llama al formulario SaleForm
        #form = SaleForm(request.POST or None, request.FILES or None)
        today = date.today() #Fecha de hoy
        if form.is_valid():
            cliente = form.cleaned_data['client'] #Extraigo los datos del formulario
            sucursal = form.cleaned_data['sucursal'] #Extraigo los datos del formulario
            sale = Sale.objects.create(
                date=today,
                client=cliente,
                sucursal=sucursal,
                seller=user,
                total=20,
            ) #Creo un objeto con los datos del formulario que será guardado en la base de datos
            sale.save() #Guardo el objeto
            return redirect('orders:create-detail', id=sale.id) #Redirigo al usuario a crear los detalles de la nota de venta
    else: #Si el metodo no es POST
        form = SaleForm(request.POST or None, request.FILES or None) #Solo llamo al formulario

    return render(request, template_name, {'form': form, })

def get_json_client_data(request):
    qs_val = list(Client.objects.values())
    return JsonResponse({'data':qs_val})

# AJAX
def load_cities(request):
    client_id = request.GET.get('client_id')
    template_name = 'sucursal-dropdown.html'
    sucursales = Sucursal.objects.filter(client_id=client_id)
    #return render(request, template_name, {'sucursales': sucursales})
    return JsonResponse(list(sucursales.values('id', 'name')), safe=False)


def listOrder(request):
    data = {}
    template_name = 'Orders.html'
    #Traigo todas las ordenes existentes, ordenadas por ID
    data['orders'] = Sale.objects.all().order_by('-id')

    if not data['orders']:
        pass
    else:
        pass

    return render(request, template_name, data)


def listDetail(request, id):
    data = {}
    template_name = 'list-detail.html'
    #Traigo los detalles de una nota de venta, manejados por el id enviado por parametro de la funcion
    data['details'] = Detail.objects.filter(Numero_boleta=id)
    if not data['details']:
        return redirect('orders:create-detail', id=id)
    else:
        data['client'] = data['details'][0].Numero_boleta.client #Cliente de la nota de venta
        data['sucursal'] = data['details'][0].Numero_boleta.sucursal #Sucursal de la nota de venta
    return render(request, template_name, data)


def updateOrder(request, id):
    template_name = 'update-order.html'
    data = {}
    #Obtengo la nota de venta mediante su ID
    order = Sale.objects.filter(pk=id)

    if request.method == 'POST':
        data['form'] = SaleForm(request.POST or None, instance=order) #Inicializo un formulario con los datos de la instancia order
        if data['form'].is_valid():
            order.save() #Si el formulario es valido, actualizo la orden
            return redirect('orders:list-orders')
    else:
        data['form'] = SaleForm(instance=order) #Inicializo un formulario con los datos de la instancia order

    return render(request, template_name, data)


    
def sendMail(request, id):
    data = {}
    template_name = 'checkout.html'
    data['sale'] = Sale.objects.get(pk=id)
    data['details'] = Detail.objects.filter(Numero_boleta=data['sale'].id) #Obtengo los detalles de la nota de venta
    #print(data['sales'])
    subject = 'Nota de venta'
    message = render_to_string('email1.html', data)
    text_content = 'xd'
    from_email = 'avisos@cbsystems.cl'
    html_content = '<h3>AAJJAJ</h3><p>{0}</p>'.format(text_content)
    """ send_mail(
        'Subject here'+str(data['sales'].id),
        'Here is the message.',
        'avisos@cbsystems.cl',
        ['cristobal.bustamante.q@gmail.com', 'xd@gmail.com'],
        fail_silently=False,
    ) #Envio mail """
    msg = EmailMultiAlternatives(
        subject,
        text_content,
        from_email,
        ['cristobal.bustamante.q@gmail.com']
    )
    pdf = render_to_pdf('email1.html', data)
    msg.attach('report.pdf', pdf)
    msg.attach_alternative(message, "text/html")
    msg.send()
    return render(request, template_name, data)

def createDetail(request, id):
    data = {}
    template_name = 'create-detail.html'
    data['sale'] = Sale.objects.get(pk=id) #Obtengo la nota de venta
    data['AIDI'] = data['sale'].id
    data['details'] = Detail.objects.filter(Numero_boleta=data['sale'].id) #Obtengo los detalles de la nota de venta

    if request.is_ajax(): #Si la request viene de ajax
        term = request.GET.get('term') #Obtengo el termino, que es un input del usuario
        products = Product.objects.all().filter(name__icontains=term) #Realizo una consulta en los productos buscando el input del usuario
        return JsonResponse(list(products.values()), safe=False)

    if request.method == 'POST':
        data['form'] = DetailForm(request.POST or None, request.FILES or None)
        if data['form'].is_valid():
            product = data['form'].cleaned_data['product'] #Extraigo los datos del formulario
            quantity = data['form'].cleaned_data['quantity'] #Extraigo los datos del formulario
            detail = Detail.objects.create(
                Numero_boleta=data['sale'],
                product=product,
                quantity=quantity
            ) #Creo un objeto con los datos del formulario que será guardado en la base de datos
            detail.save() #Guardo el objeto en la base de datos
            return redirect('orders:create-detail', id=data['sale'].id) #Redirigo al usuario a crear los detalles de la nota de venta
    else:
        data['form'] = DetailForm()
        #sendMail(request, id=data['sale'].id)
    return render(request, template_name, data)


def updateDetail(request, id):
    template_name = 'update-detail.html'
    data = {}
    detail = Detail.objects.get(pk=id)

    if request.method == 'POST':
        data['form'] = DetailForm(request.POST or None, instance=detail) #Inicializo un formulario con los datos de la instancia detail
        if data['form'].is_valid():
            detail.save() #Actualizo el detalle
            return redirect('orders:create-detail', id=detail.Numero_boleta.id) #Redirigo al usuario a crear los detalles de la nota de venta
    else:
        data['form'] = DetailForm(instance=detail)

    return render(request, template_name, data)


def deleteDetail(request, id):
    template_name = 'delete-detail.html'
    data = {}
    data['detail'] = Detail.objects.get(pk=id) #Busco el detalle por el id que viene como parametro de la funcion
    boleta = data['detail'].Numero_boleta.id #Extraigo el id de la nota de venta del detalle
    if request.method == 'POST':
        data['detail'].delete() #Elimino el detalle de la nota de venta
        return redirect('orders:create-detail', id=boleta)  #Redirigo al usuario a crear mas detalles de la boleta REVISAR!! 

    return render(request, template_name, data)


def export_csv(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=orders' + \
        str(datetime.datetime.now())+'.csv' #Creo un archivo csv con la fecha de hoy
    writer = csv.writer(response)
    writer.writerow(['ID', 'Cliente', 'Sucursal',
                    'Fecha', 'Producto', 'Cantidad']) #Columnas del CSV
    details = Detail.objects.all() #Query que será ingresada al csv
    for detail in details:
        writer.writerow([detail.Numero_boleta.id, detail.Numero_boleta.date, detail.Numero_boleta.client,
                        detail.Numero_boleta.sucursal, detail.product, detail.quantity]) #Inserto todas las ocurrencias de la query en el CSV
    return response


